from models import *


def read_input(file):
    params = {}
    with open(file, 'r') as input_file:
        line = input_file.readline()
        general = line.split(' ')
        params['rows'] = int(general[0])
        # number of rows on the map
        params['cols'] = int(general[1])
        # number of columns on the map
        params['ndrones'] = int(general[2])
        # number of available drones
        params['deadline'] = int(general[3])
        # the deadline
        params['max_load'] = int(general[4])
        # maximum load of the drones
        params['nproducts'] = int(input_file.readline())
        # the number of different products
        params['weights'] = {}
        # a list of the products' weight --> params['products_weight'][54] = weight of the product type 54
        products_weights = input_file.readline().split(' ')
        for i in range(params['nproducts']):
                params['weights'][i] = int(products_weights[i])
        params['warehouses'] = []
        # the list of the warehouses
        for i in range(int(input_file.readline())):
            line = input_file.readline().split(' ')
            pos = (int(line[0]), int(line[1]))
            warehouse = Warehouse(pos, {}, i)
            products = input_file.readline().split(' ')
            for i in range(params['nproducts']):
                warehouse.items[i] = int(products[i])
            params['warehouses'].append(warehouse)
        params['orders'] = []
        # the list of the orders
        # order.pos = position of delivery
        # order.products = a dict of the needed products
        for i in range(int(input_file.readline())):
            line = input_file.readline().split(' ')
            x = int(line[0])
            y = int(line[1])
            order = Order((x, y), {}, i)
            input_file.readline()
            products = input_file.readline().split(' ')
            for p in products:
                if int(p) in order.products:
                    order.products[int(p)] += 1
                else:
                    order.products[int(p)] = 1
            params['orders'].append(order)
        params['actions'] = []
        params['drones'] = []
        for i in range(params['ndrones']):
            drone = Drone(i, {}, (0,0))
            params['drones'].append(drone)

    return params


def add_action(actions, drone, action_type, *args):
    action = [str(drone), str(action_type)]
    for a in args:
        action.append(str(a))
    action = " ".join(action)
    actions.append(action)


def output(actions, file):
    with open(file, 'w') as output_file:
        output_file.write(str(len(actions)) + "\n")
        for action in actions:
            output_file.write(action + "\n")

