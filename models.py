import math


def add_action(actions, drone, action_type, *args):
    action = [str(drone), str(action_type)]
    for a in args:
        action.append(str(a))
    action = " ".join(action)
    actions.append(action)


def output(actions, file):
    with open(file, 'w') as output_file:
        output_file.write(str(len(actions)) + "\n")
        for action in actions:
            output_file.write(action + "\n")

def get_distance(first, second):
        return math.ceil(
            math.sqrt(
                abs(first[0] - second[0]) ** 2 + abs(first[1] - second[1]) ** 2
            )
        )


class Drone(object):

    def __init__(self, number, products, pos):
        self.number = number
        self.products = {}
        self.pos = pos

    def can_load_item(self, terrain, productType, n):
        weight = terrain['weights'][productType] * n
        return weight <= terrain['max_load'] - self.get_weight(terrain)

    def get_weight(self, terrain):
        weight = 0
        for p in self.products:
            weight += self.products[p] * terrain['weights'][p]
        return weight

    def load(self, terrain, productType, n):
        available = []
        for warehouse in terrain['warehouses']:
            if warehouse.item_is_available(productType, n):
                available.append(warehouse)
        shortest_warehouse = available[0]
        shortest_dist = 0
        for warehouse in available:
            dist = get_distance(self.pos, warehouse.pos)
            if dist < shortest_dist:
                shortest_warehouse = warehouse
                shortest_dist = dist
        if self.can_load_item(terrain, productType, n):
            if productType in self.products:
                self.products[productType] += n
            else:
                self.products[productType] = n
            add_action(terrain['actions'], self.number, 'L', shortest_warehouse.number, productType, n)
        else:
            a = n
            for i in range(n):
                if productType in self.products:
                    self.products[productType] += 1
                else:
                    self.products[productType] = 1
                add_action(terrain['actions'], self.number, 'L', shortest_warehouse.number, productType, 1)


    def deliver(self, terrain, productType, n, pos, order):
        """
        Deliver to customer.
        :param pos Tuple of coordinates
        """
        dist = get_distance(self.pos, pos)


        self.products[productType] -= n
        add_action(terrain['actions'], self.number, 'D', order.number, productType, n)


class ProductItem(object):

    def __init__(self, product_type, weight, stock):
        self.productType = product_type
        self.weight = weight
        self.stock = stock


class Warehouse(object):

    def __init__(self, pos, items, number):
        self.items = items
        self.pos = pos
        self.number = number

    def item_is_available(self, productType, n):
        return self.items[productType] >= n



class Customer(object):

    def __init__(self, pos):
        self.pos = pos


class Terrain(object):

    def __init__(self, warehouses, customers):
        # self.matrix = []
        self.warehouses = warehouses
        self.customers = customers
        self.drones = []
        self.orders = []

    def build_terrain(self, warehouses, customers, drones):
        pass

    def process_order_item(self, item):
        for warehouse in self.warehouses:
            if warehouse.item_is_available(item):
                return warehouse
        return None


class Order(object):

    def __init__(self, pos, products, number):
        self.number = number
        self.pos = pos
        self.products = products


if __name__ == "__main__":
    p1 = ProductItem(1, 1, 1)
    p2 = ProductItem(2, 2, 2)
    p3 = ProductItem(3, 3, 3)
    p4 = ProductItem(4, 3, 0)

    w1 = Warehouse((1, 2), [p1, p2, p3])
    w2 = Warehouse((2, 2), [p1, p2, p4])

    c1 = Customer((3, 2))
    c2 = Customer((7, 3))

    t = Terrain([c1, c2], [w1, w2])
