
from in_out import *


def handle_input(input_file):
    params = read_input(input_file)
    for o in params['orders']:
        for p in o.products:
            for i in range(o.products[p]):
                params['drones'][0].load(params, p, o.products[p])
                params['drones'][0].deliver(params, p, o.products[p], o.pos, o)
    output(params['actions'], input_file + ".out")

if __name__ == "__main__":
    handle_input("input/busy_day.in")
    handle_input("input/mother_of_all_warehouses.in")
    handle_input("input/redundancy.in")
